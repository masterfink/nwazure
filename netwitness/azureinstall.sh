#!/usr/bin/env bash
# Author - Taylor Finklea
# This program was written to make the process of setting up NetWitness in Azure easier
version="v0.00.01"


# Check for root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi


# Gather variables for the script
echo "#### NetWitness Azure Installation Script $version ####"
echo ""

PS3="Please Select a System to Install: "
echo ""
options=("Head/Broker" "ESA" "Log Decoder" "Packet Decoder" "Concentrator" "UEBA" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Head/Broker")
	    echo ""
	    bash head.sh
	    break
            ;;
        "ESA")
        echo ""
	    bash esa.sh
	    break
            ;;
        "Log Decoder")
	    echo ""
        bash logdec.sh
	    break
            ;;
	    "Packet Decoder")
	    echo ""
        bash packdec.sh
	    break
            ;;
	    "Concentrator")
	    echo ""
        bash con.sh
	    break
            ;;
	    "UEBA")
	    echo ""
        echo "Sucks for you, this has not been written yet!"
	    break
            ;;
        "Quit")
	    echo "Bye"
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
