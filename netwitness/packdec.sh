#!/usr/bin/env bash
# Author - Taylor Finklea
# Check for root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Configure NWHome
echo "#### Installer for Packet Decoder ####"
echo ""
lsblk
echo ""
read -p "Which disk is for your /var/netwitness volume? (ex. /dev/sdc): " nwVol
lsblk $nwVol
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " nwVolSize
# Remove 5G for good measure
nwVolSize=$(expr $nwVolSize - 5)
nwVolSize="${nwVolSize}G"
pvcreate $nwVol
vgextend netwitness_vg00 $nwVol
lvextend -L $nwVolSize /dev/netwitness_vg00/nwhome
xfs_growfs /dev/netwitness_vg00/nwhome

# Configure decodersmall
echo "##### Configure decodersmall ######"
echo ""
lsblk
echo ""
echo "######################################"
echo "We will now configure your smaller RAID array md0"
read -p "Enter the path to your first disk (ex. /dev/sdd): " md0Pv0
read -p "Enter the path to your second disk (ex. /dev/sde): " md0Pv1
# Create the RAID array md0
mdadm --create /dev/md0 --assume-clean --level 0 --raid-devices=2 $md0Pv0 $md0Pv1
pvcreate /dev/md0
lsblk /dev/md0
# Calcuate sizes
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " md0VolSize
## To get size of sessiondb, minus 10G for decoroot, 30G for index, 370G metadb and an extra 5G for good measure
md0Sessiondb=$(expr $md0VolSize - 415)
md0Sessiondb="${md0Sessiondb}G"
# Create Logical Volumes
vgcreate -s 32 decodersmall /dev/md0
## decoroot
lvcreate -L 10G -n decoroot decodersmall
mkfs.xfs /dev/decodersmall/decoroot
## index
lvcreate -L 30G -n index decodersmall
mkfs.xfs /dev/decodersmall/index
## metadb
lvcreate -L 370G -n metadb decodersmall
mkfs.xfs /dev/decodersmall/metadb
## sessiondb
lvcreate -L $md0Sessiondb -n sessiondb decodersmall
mkfs.xfs /dev/decodersmall/sessiondb
mdadm --detail --scan > /etc/mdadm.conf

# Configure decoder
echo "##### Configure decoder ######"
echo ""
lsblk
echo ""
echo "######################################"
echo "We will now configure your larger RAID array md1"
read -p "Enter the path to your first disk (ex. /dev/sdf): " md1Pv0
read -p "Enter the path to your second disk (ex. /dev/sdg): " md1Pv1
# Create the RAID array md1
mdadm --create /dev/md1 --assume-clean --level 0 --raid-devices=2 $md1Pv0 $md1Pv1
pvcreate /dev/md1
lsblk /dev/md1
# Calcuate sizes
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " md1VolSize
# Remove 5G for good measure
md1VolSize=$(expr $md1VolSize - 5)
md1VolSize="${md1VolSize}G"
# Create Logical Volumes
vgcreate -s 32 decoder /dev/md1
## packetdb
lvcreate -L $md1VolSize -n packetdb decoder
mkfs.xfs /dev/decoder/packetdb
mdadm --detail --scan > /etc/mdadm.conf

# Set up folders and mount LVM volumes
mkdir /var/netwitness/decoder
echo "/dev/decodersmall/decoroot /var/netwitness/decoder xfs noatime,nosuid 1 2" >> /etc/fstab
mount -a
mkdir /var/netwitness/decoder/index
mkdir /var/netwitness/decoder/metadb
mkdir /var/netwitness/decoder/sessiondb
mkdir /var/netwitness/decoder/packetdb
echo "
/dev/decodersmall/index /var/netwitness/decoder/index xfs noatime,nosuid 1 2
/dev/decodersmall/metadb /var/netwitness/decoder/metadb xfs noatime,nosuid 1 2
/dev/decodersmall/sessiondb /var/netwitness/decoder/sessiondb xfs noatime,nosuid 1 2
/dev/decoder/packetdb /var/netwitness/decoder/packetdb xfs noatime,nosuid 1 2" >> /etc/fstab
mount -a

echo ""
echo "######################################"
echo "Done! Make sure your drives look good and then run nwsetup-tui"
echo ""
echo ""
df -h
