#!/usr/bin/env bash
# Author - Taylor Finklea
# Check for root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Configure NWHome
echo "#### Installer for Head Unit ####"
echo ""
lsblk
echo ""
read -p "Which disk is for your /var/netwitness volume? (ex. /dev/sdc): " nwVol
lsblk $nwVol
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " nwVolSize
# Remove 5G for good measure
nwVolSize=$(expr $nwVolSize - 5)
nwVolSize="${nwVolSize}G"
pvcreate $nwVol
vgextend netwitness_vg00 $nwVol
lvextend -L $nwVolSize /dev/netwitness_vg00/nwhome
xfs_growfs /dev/netwitness_vg00/nwhome

echo ""
echo "######################################"
echo "Done! Make sure your drives look good and then run nwsetup-tui"
echo ""
echo ""
df -h