#!/usr/bin/env bash
# Author - Taylor Finklea
# Check for root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Configure NWHome
echo "#### Installer for Concentrator ####"
echo ""
lsblk
echo ""
read -p "Which disk is for your /var/netwitness volume? (ex. /dev/sdc): " nwVol
lsblk $nwVol
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " nwVolSize
# Remove 5G for good measure
nwVolSize=$(expr $nwVolSize - 5)
nwVolSize="${nwVolSize}G"
pvcreate $nwVol
vgextend netwitness_vg00 $nwVol
lvextend -L $nwVolSize /dev/netwitness_vg00/nwhome
xfs_growfs /dev/netwitness_vg00/nwhome

# Configure concentrator
echo "##### Configure concentrator ######"
echo ""
lsblk
echo ""
echo "######################################"
echo "We will now configure your larger RAID array md0"
read -p "Enter the path to your first disk (ex. /dev/sdd): " md0Pv0
read -p "Enter the path to your second disk (ex. /dev/sde): " md0Pv1
# Create the RAID array md0
mdadm --create /dev/md0 --assume-clean --level 0 --raid-devices=2 $md0Pv0 $md0Pv1
pvcreate /dev/md0
lsblk /dev/md0
# Calcuate sizes
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " md0VolSize
## To get size of metadb, minus 30G for root, 2TB for sessiondb and an extra 5G for good measure
md0Metadb=$(expr $md0VolSize - 2083)
md0Metadb="${md0Metadb}G"
# Create Logical Volumes
vgcreate -s 32 concentrator /dev/md0
## root
lvcreate -L 30G -n root concentrator
mkfs.xfs /dev/concentrator/root
## sessiondb
lvcreate -L 2048G -n sessiondb concentrator
mkfs.xfs /dev/concentrator/sessiondb
## metadb
lvcreate -L $md0Metadb -n metadb concentrator
mkfs.xfs /dev/concentrator/metadb
mdadm --detail --scan > /etc/mdadm.conf

# Configure index
echo "##### Configure index ######"
echo ""
lsblk
echo ""
echo "######################################"
echo "We will now configure your smaller RAID array md1"
echo "NOTE: This is the SSD"
read -p "Enter the path to your first disk (ex. /dev/sdf): " md1Pv0
read -p "Enter the path to your second disk (ex. /dev/sdg): " md1Pv1
# Create the RAID array md1
mdadm --create /dev/md1 --assume-clean --level 0 --raid-devices=2 $md1Pv0 $md1Pv1
pvcreate /dev/md1
lsblk /dev/md1
## Calcuate sizes
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " md1VolSize
## Remove 5G for good measure
md1VolSize=$(expr $md1VolSize - 5)
md1VolSize="${md1VolSize}G"
# Create Logical Volumes
vgcreate -s 32 index /dev/md1
## index
lvcreate -L $md1VolSize -n index index
mkfs.xfs /dev/index/index
mdadm --detail --scan > /etc/mdadm.conf

# Set up folders and mount LVM volumes
mkdir /var/netwitness/concentrator
echo "/dev/concentrator/root /var/netwitness/concentrator xfs noatime,nosuid 1 2" >> /etc/fstab
mount -a
mkdir /var/netwitness/concentrator/sessiondb
mkdir /var/netwitness/concentrator/metadb
mkdir /var/netwitness/concentrator/index
echo "
/dev/concentrator/sessiondb /var/netwitness/concentrator/sessiondb xfs noatime,nosuid 1 2
/dev/concentrator/metadb /var/netwitness/concentrator/metadb xfs noatime,nosuid 1 2 2
/dev/index/index /var/netwitness/concentrator/index xfs noatime,nosuid 1 2
" >> /etc/fstab
mount -a

echo ""
echo "######################################"
echo "Done! Make sure your drives look good and then run nwsetup-tui"
echo ""
echo ""
df -h
