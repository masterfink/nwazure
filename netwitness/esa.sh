#!/usr/bin/env bash
# Author - Taylor Finklea

# Check for root
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Configure NWHome
echo "#### Installer for ESA ####"
echo ""
lsblk
echo ""
read -p "Do you need to set up a RAID array? (y n): " -n 1 -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
echo "##### Configure RAID Array ######"
echo ""
lsblk
echo ""
echo "######################################"
echo "We will now configure your larger RAID array md1"
read -p "Enter the path to your first disk (ex. /dev/sdf): " md1Pv0
read -p "Enter the path to your second disk (ex. /dev/sdg): " md1Pv1
# Create the RAID array md1
mdadm --create /dev/md1 --assume-clean --level 0 --raid-devices=2 $md1Pv0 $md1Pv1
pvcreate /dev/md1
lsblk /dev/md1
# Calcuate sizes
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " md1VolSize
# Remove 5G for good measure
md1VolSize=$(expr $md1VolSize - 5)
md1VolSize="${md1VolSize}G"
vgextend netwitness_vg00 /dev/md1
lvextend -L $md1VolSize /dev/netwitness_vg00/nwhome
xfs_growfs /dev/netwitness_vg00/nwhome
mdadm --detail --scan > /etc/mdadm.conf

else
read -p "Which disk is for your /var/netwitness volume? (ex. /dev/sdc): " nwVol
lsblk $nwVol
read -p "How large is the drive in Gigabytes? Do Not Include the GB (ex. 2048): " nwVolSize
## Remove 5G for good measure
nwVolSize=$(expr $nwVolSize - 5)
nwVolSize="${nwVolSize}G"
pvcreate $nwVol
vgextend netwitness_vg00 $nwVol
lvextend -L $nwVolSize /dev/netwitness_vg00/nwhome
xfs_growfs /dev/netwitness_vg00/nwhome
fi

echo ""
echo "######################################"
echo "Done! Make sure your drives look good and then run nwsetup-tui"
echo ""
echo ""
df -h